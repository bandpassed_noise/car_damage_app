FROM python:3.9.7
RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6 -y
COPY . /app
WORKDIR /app
RUN rm -rf venv
RUN rm -rf yolov5l6.pt
RUN rm -rf windows_instr.txt
RUN pip install -r requirements.txt
EXPOSE 8000
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]