import uvicorn  # pip list --format=freeze > requirements.txt
import cv2  # pip install opencv-python
import numpy as np  # also python -m pip install -U matplotlib
import torch  # also pip install pyaml, torchvision, pandas, tqdm, seaborn
from fastapi import File, UploadFile, FastAPI  # also pip install python-multipart (form data requirement)
from models_aux.auxilary import s3_get_h5_model, image_prediction, yolo_and_crop
# from keras.models import load_model

app = FastAPI()

# load models: yolo and damage detection
tf_model = s3_get_h5_model('model_tuner_base_aug.h5')  # problematic when read from s3
torch_model = torch.hub.load('ultralytics/yolov5', 'yolov5l6')  # or yolov5n - yolov5x6, custom


@app.post('/upload')
async def upload(file: UploadFile = File(...)):
    try:
        contents = await file.read()
        nparr = np.fromstring(contents, np.uint8)
        image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

        yoloed_image = yolo_and_crop(image, torch_model, 250)  # the damage model accepts 250x250 images

        prediction = image_prediction(yoloed_image, tf_model)  # single image prediction
        prediction = round(prediction[0][0], 5)  # unnest and round
        predict_str = format(prediction, 'f')  # no scientific notation
    except Exception:
        return {'message': 'There was an error uploading and processing the file'}
    finally:
        await file.close()

    return {"message": f'upload success {file.filename}, img shape: {image.shape}, damaged probability: {predict_str}'}


if __name__ == '__main__':
    uvicorn.run(app, host='127.0.0.1', port=8000)
