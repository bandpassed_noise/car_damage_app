# for venv activation
Set-ExecutionPolicy RemoteSigned
venv/Scripts/activate.ps1

# docker
docker build . -t car_dmg

docker tag car_dmg:latest bandpassednoise/dockerhub:car_dmg
docker push bandpassednoise/dockerhub:car_dmg

# reqs
pip list --format=freeze > requirements.txt