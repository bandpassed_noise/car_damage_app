import os
import numpy as np
import cv2
import boto3
import tempfile
import zipfile
import logging
import shutil  # zip save
from keras.models import load_model, Model  # also pip install tensorflow
from dotenv import load_dotenv  # pip3 install python-dotenv

load_dotenv()

AWS_ACCESS_KEY = os.getenv('AWS_ACCESS_KEY')
AWS_SECRET_KEY = os.getenv('AWS_SECRET_KEY')
# BUCKET_NAME = os.getenv('BUCKET_NAME')
BUCKET_NAME = 'elasticbeanstalk-us-west-2-303518111465'


def get_s3():
    return boto3.client('s3',
                        aws_access_key_id=AWS_ACCESS_KEY,
                        aws_secret_access_key=AWS_SECRET_KEY)


def zip_model(keras_model, mdl_path: str, mdl_name: str):
    keras_model.save(os.path.join(mdl_path, mdl_name))
    # Zip it up first
    shutil.make_archive(os.path.join(mdl_path, mdl_name), 'zip', os.path.join(mdl_path, mdl_name))


def s3_upload_file(s3_folder, local_file_path):
    s3_client = get_s3()
    s3_path = os.path.join(s3_folder, os.path.basename(local_file_path))
    with open(local_file_path, mode='rb') as data:
        s3_client.upload_fileobj(data, BUCKET_NAME, s3_path)
    logging.info(f'Saved zipped model at path models_aux://{BUCKET_NAME}/{os.path.basename(local_file_path)}.zip')


def s3_get_zip_model(model_name: str) -> Model:
    with tempfile.TemporaryDirectory() as tempdir:
        s3 = get_s3()
        mdl_path = f'{tempdir}/{model_name}'
        # Fetch and save the zip file to the temporary directory
        s3.download_file(BUCKET_NAME, 'models/' + model_name, f'{mdl_path}.zip')
        # Extract the model zip file within the temporary directory
        with zipfile.ZipFile(f'{mdl_path}.zip') as zip_ref:
            zip_ref.extractall(mdl_path)
        # Load the keras model from the temporary directory
        return load_model(mdl_path)


def s3_get_h5_model(model_name: str) -> Model:
    tmp = tempfile.mkdtemp()
    s3 = get_s3()
    mdl_path = os.path.join(tmp, model_name)
    # Fetch and save the h5 file to the temporary directory
    s3.download_file(BUCKET_NAME, 'models/' + model_name, mdl_path)
    # Load the keras model from the temporary directory
    return load_model(mdl_path)


def image_prediction(image_array, keras_model):
    image_rescaled = image_array / 255.0  # between 0 and 1
    image_rescaled = image_rescaled[np.newaxis, ...]  # add dim to be able to predict
    result = keras_model.predict(image_rescaled)
    result = 1 - result  # inverse probability (so 1 would be damaged)
    return result


# function for cropping each detection, returning the largest one and resizing to a specified size in px
def yolo_and_crop(image_array, torch_model, resize_px: int):
    yolo_res = torch_model(image_array)
    yolo_pd = yolo_res.pandas().xyxy[0]

    num_objects = yolo_pd.shape[0]
    boxes = yolo_pd.iloc[:, 0:4]
    # scores = yolo_pd.iloc[:, 4]  # unused results
    # class_names = yolo_pd.iloc[:, 6]
    # create dictionary to hold count of objects for image name
    img_list = []
    dims_mult = []
    for i in range(num_objects):
        # get box coordinates
        xmin, ymin, xmax, ymax = boxes.iloc[i]
        # crop detection from image (take an additional 5 pixels around all edges)
        cropped_img = image_array[int(ymin) - 0:int(ymax) + 0, int(xmin) - 0:int(xmax) + 0]  # 0 px added to the frame
        mult = cropped_img.shape[0] * cropped_img.shape[1]
        img_list.append(cropped_img)
        dims_mult.append(mult)

    largest_img_indx = dims_mult.index(max(dims_mult))
    largest_img = img_list[largest_img_indx]
    result = cv2.resize(largest_img, (resize_px, resize_px))
    return result
